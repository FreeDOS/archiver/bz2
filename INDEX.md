# BZIP2

The BZIP2 compression program.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## BZ2.LSM

<table>
<tr><td>title</td><td>BZIP2</td></tr>
<tr><td>version</td><td>1.0.8a</td></tr>
<tr><td>entered&nbsp;date</td><td>2020-04-18</td></tr>
<tr><td>description</td><td>BZIP2 compression program</td></tr>
<tr><td>keywords</td><td>bzip, bunzip, zip, unzip, packer</td></tr>
<tr><td>author</td><td>Julian R. Seward jseward-at-bzip.org</td></tr>
<tr><td>maintained&nbsp;by</td><td>BZIP2 project</td></tr>
<tr><td>primary&nbsp;site</td><td>http://ftp.delorie.com/pub/djgpp/current/v2apps/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://sourceforge.net/projects/bzip2/</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.bzip.org/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS, Linux, Windows</td></tr>
<tr><td>copying&nbsp;policy</td><td>Open Source, see license file</td></tr>
<tr><td>summary</td><td>The BZIP2 compression program.</td></tr>
</table>

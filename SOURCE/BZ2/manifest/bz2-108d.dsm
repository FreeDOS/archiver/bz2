#
# DSM for Bzip2 1.0.8 documentation
# Written by Juan Manuel Guerrero <juan.guerrero@gmx.de> 2020-04-18
# Product compiled using gcc346b, bnu234b and djdev206.
#

dsm-file-version: 1.0
dsm-name: bz2-108d
dsm-version: 0.6.1

type: documentation

dsm-author: Juan Manuel Guerrero
dsm-author-email: juan.guerrero@gmx.de

name: bzip2
version: 1.0.8 release 1
manifest: bz2-108d
binaries-dsm: bz2-108a
binaries-dsm: bz2-108b
documentation-dsm: bz2-108d
sources-dsm: bz2-108s
short-description: Documentation for Bzip2 version 1.0.8.
long-description: Bzip2 compresses files using the Burrows-Wheeler\n\
block sorting text compression algorithm, and Huffman coding.\n\
The command-line options are deliberately very similar to\n\
those of GNU gzip, but they are not identical.

license: BSD style License
author: Julian Seward
author-email: jseward@acm.org
maintainer: Julian Seward
maintainer-email: jseward@acm.org
# For reporting bugs.
mailing-list: bzip2-devel@sourceware.org
# The home page of Bzip2.
web-site: https://www.sourceware.org/bzip2
# The ftp site of Bzip2.
ftp-site: ftp://sourceware.org/pub/bzip2
# For discussing DJGPP- and DOS/Windows-specific aspects.
mailing-list: djgpp@delorie.com
newsgroup: comp.os.msdos.djgpp

porter: Juan Manuel Guerrero
porter-email: juan.guerrero@gmx.de

simtelnet-path: v2apps/
zip: bz2-108d.zip

replaces: bzip2 < 1.0.8 release 1
